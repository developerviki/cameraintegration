package com.example.cameraintegration;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.widget.ImageView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Camera2AutoActivity extends AppCompatActivity {

    private final static String TAG = "Camera2AutoActivity";
    private ArrayList<String> allCameraIds = new ArrayList<>();
    private String mCameraId;
    private CameraDevice mCameraDevice;
    private CameraCaptureSession mCameraCaptureSession;
    Surface mSurface;
    List<Surface> mSurfaceList = new ArrayList<>();
    ImageReader mImageReader;
    ImageView iv1, iv2, iv3, iv4, iv5;
    ArrayList<ImageView> ivArray = new ArrayList<>();
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_auto);

        //Check if we have Camera permissions
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityResultLauncher requestPermissionLauncher = registerForActivityResult(
                    new ActivityResultContracts.RequestPermission(), permissionCallback);
            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
        }

        iv1 = findViewById(R.id.iv1);
        iv2 = findViewById(R.id.iv2);
        iv3 = findViewById(R.id.iv3);
        iv4 = findViewById(R.id.iv4);
        iv5 = findViewById(R.id.iv5);
        ivArray.add(iv1);
        ivArray.add(iv2);
        ivArray.add(iv3);
        ivArray.add(iv4);
        ivArray.add(iv5);

        CameraManager manager = (CameraManager)this.getSystemService(Context.CAMERA_SERVICE);
        String[] cameraIdList;
        try {
            if (manager != null) {
                cameraIdList = manager.getCameraIdList();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    filterCameraIdsFacing(cameraIdList, manager, CameraMetadata.LENS_FACING_EXTERNAL);
                }
                filterCameraIdsFacing(cameraIdList, manager, CameraMetadata.LENS_FACING_BACK);
                filterCameraIdsFacing(cameraIdList, manager, CameraMetadata.LENS_FACING_FRONT);
                for(String cameraId : allCameraIds) {
                    Log.d(TAG, cameraId);
                    mCameraId = cameraId;
                    break;
                }

                mImageReader = ImageReader.newInstance(720, 1080, ImageFormat.JPEG, 1);
                mImageReader.setOnImageAvailableListener(mImageAvailableListener, null);
                mSurface = mImageReader.getSurface();
                mSurfaceList.add(mSurface);
                if(mCameraId != null)
                    manager.openCamera(mCameraId, cameraDeviceStateCallback, null);

                UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                HashMap<String, UsbDevice> deviceList = null;
                if (usbManager != null) {
                    deviceList = usbManager.getDeviceList();
                    Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
                    while(deviceIterator.hasNext()) {
                        UsbDevice device = deviceIterator.next();
                        Log.d(TAG, device.toString());
                        Toast.makeText(this, device.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                boolean external_camera_support = this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_EXTERNAL);
                Log.d(TAG, "external camera support-> " + external_camera_support);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "IAE", e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mCameraCaptureSession) {
            mCameraCaptureSession.close();
            mCameraCaptureSession = null;
        }
        if (null != mCameraDevice) {
            mCameraDevice.close();
            mCameraDevice = null;
        }
        if (null != mImageReader) {
            mImageReader.close();
            mImageReader = null;
        }
    }

    ImageReader.OnImageAvailableListener mImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Image image = reader.acquireNextImage();
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            final Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
            if(ivArray.get(count) != null) {
                ivArray.get(count++).setImageBitmap(rotateImage90(bitmap));
            }
            image.close();
        }
    };

    CameraCaptureSession.CaptureCallback mCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
        }
    };

    CameraCaptureSession.StateCallback cameraCaptureSessionStateCallback = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            mCameraCaptureSession = session;
            CaptureRequest.Builder captureRequestBuilder = null;
            try {
                captureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
                captureRequestBuilder.addTarget(mSurface);

                mCameraCaptureSession.capture(captureRequestBuilder.build(), mCaptureCallback, null);
                mCameraCaptureSession.capture(captureRequestBuilder.build(), mCaptureCallback, null);
                mCameraCaptureSession.capture(captureRequestBuilder.build(), mCaptureCallback, null);
                mCameraCaptureSession.capture(captureRequestBuilder.build(), mCaptureCallback, null);
                mCameraCaptureSession.capture(captureRequestBuilder.build(), mCaptureCallback, null);
            } catch (CameraAccessException e) {
                Log.e(TAG, "IAE", e);
            }
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
            mCameraCaptureSession = null;
        }
    };

    CameraDevice.StateCallback cameraDeviceStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            mCameraDevice = camera;

            try {
                mCameraDevice.createCaptureSession(mSurfaceList, cameraCaptureSessionStateCallback, null);
            } catch (CameraAccessException e) {
                Log.e(TAG, "CAE", e);
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            camera.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            camera.close();
            mCameraDevice = null;
        }
    };

    private void filterCameraIdsFacing(String[] cameraIdList, CameraManager  manager, int facing) throws CameraAccessException{
        for(String cameraId : cameraIdList) {
            CameraCharacteristics cameraCharacteristics = manager.getCameraCharacteristics(cameraId);
            Integer lensFacingInt = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
            if(((Integer) facing).equals(lensFacingInt)){
                allCameraIds.add(cameraId);
            }
        }
    }

    private Bitmap rotateImage90(Bitmap img) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    ActivityResultCallback permissionCallback = new ActivityResultCallback<Boolean>() {
        @Override
        public void onActivityResult(Boolean result) {
            if (result == Boolean.FALSE) {
                Log.d(TAG, "Permission denied, therefore exiting");
                finish();
            }
            else if (result == Boolean.TRUE) {
                Log.d(TAG, "User granted permission");
            }
        }
    };
}
