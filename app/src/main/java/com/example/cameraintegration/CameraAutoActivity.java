package com.example.cameraintegration;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import com.google.common.util.concurrent.ListenableFuture;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CameraAutoActivity extends AppCompatActivity implements ImageCapture.OnImageSavedCallback {

    private static final String TAG = "CameraAutoActivity";

    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    ImageCapture imageCapture;

    ImageView iv1, iv2, iv3, iv4, iv5;
    ArrayList<ImageView> ivArray = new ArrayList<>();
    int count = 0;
    ExecutorService singleThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_auto);

        //Check if we have Camera permissions
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityResultLauncher requestPermissionLauncher = registerForActivityResult(
                    new ActivityResultContracts.RequestPermission(), permissionCallback);
            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
        }

        iv1 = findViewById(R.id.iv1);
        iv2 = findViewById(R.id.iv2);
        iv3 = findViewById(R.id.iv3);
        iv4 = findViewById(R.id.iv4);
        iv5 = findViewById(R.id.iv5);
        ivArray.add(iv1);
        ivArray.add(iv2);
        ivArray.add(iv3);
        ivArray.add(iv4);
        ivArray.add(iv5);
        singleThread = Executors.newSingleThreadExecutor();

        //Initialize Camera Provider Future
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                bindPreview(cameraProvider);
                capture();
            } catch (ExecutionException | InterruptedException e) {
                Log.e(TAG, "Exception", e);
            }
        }, ContextCompat.getMainExecutor(this));
    }

    public void bindPreview(ProcessCameraProvider cameraProvider) {
        CameraSelector cameraSelector = new CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build();

        imageCapture = new ImageCapture.Builder().setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY).build();
        cameraProvider.unbindAll();
        Camera c = cameraProvider.bindToLifecycle(this, cameraSelector, imageCapture);
    }

    public void capture() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "NEW_IMAGE");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
        ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions
                .Builder(getContentResolver(),MediaStore.Images.Media.EXTERNAL_CONTENT_URI,contentValues)
                        .build();
        imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(this), this);
        imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(this), this);
        imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(this), this);
        imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(this), this);
        imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(this), this);
    }

    @Override
    public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
        try {
            Uri uri = outputFileResults.getSavedUri();
            if(uri != null) {
                ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
                if(parcelFileDescriptor != null) {
                    FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                    Bitmap image = rotateImage90(BitmapFactory.decodeFileDescriptor(fileDescriptor));
                    Bitmap scaledImage = Bitmap.createScaledBitmap(image, 720, 1080, false);
                    parcelFileDescriptor.close();
                    ivArray.get(count++).setImageBitmap(scaledImage);
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Exception", e);
        }
    }

    // No exif details, framework assumes device is in landscape mode
    private Bitmap rotateImage90(Bitmap img) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    @Override
    public void onError(@NonNull ImageCaptureException exception) {
    }

    ActivityResultCallback permissionCallback = new ActivityResultCallback<Boolean>() {
        @Override
        public void onActivityResult(Boolean result) {
            if (result == Boolean.FALSE) {
                Log.d(TAG, "Permission denied, therefore exiting");
                finish();
            }
            else if (result == Boolean.TRUE) {
                Log.d(TAG, "User granted permission");
            }
        }
    };
}
